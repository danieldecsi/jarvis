# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [6.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.3.0...v6.0.0) (2021-09-14)


### ⚠ BREAKING CHANGES

* refactor Git key and identity handling
* migrate project to OSS version hosted on gitlab.com

### Features

* migrate project to OSS version hosted on gitlab.com ([ceee152](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ceee1522581bf54726d4c082a71303a320c04cd7))
* move separate files to a single jarvis.yml ([e22f40d](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/e22f40db5f4a77cff0ab6169a11ae67c543c7d83))
* refactor Git key and identity handling ([ceba8e7](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ceba8e7e80676bce6c6faf57a0ddce95dec24655))
* upgrade to latest builds of helper images ([8d06049](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/8d06049907c11579df08d3cfbf1f56682bcaf91d))

## [5.3.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.2.0...v5.3.0) (2021-05-14)


### Features

* add .jarvis-extract-release-changes-since-deployment template ([4794b9f](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/4794b9f7bd14b86dfd6f5e023f1381e644de63bc))


### Bug Fixes

* do not run release jobs on scheduled pipelines ([62935ce](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/62935cee42490fe7008f490873d5ada6069f40c7))

## [5.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.1.0...v5.2.0) (2021-04-12)


### Features

* use default branch instead of hardcoding 'master' ([367fdd9](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/367fdd9f7509c5fbbeb1f1e0470decd4ad400950))

## [5.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.0.0...v5.1.0) (2021-03-19)


### Features

* allow overriding release brnach rules for main release ([0811954](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/081195489d6483827f04cd2a1ae202cd34ecdfb6))

## [5.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v4.2.0...v5.0.0) (2021-03-17)


### ⚠ BREAKING CHANGES

* remove helper image sources from repo
* remove pre- prefix from pre-release tags

### Features

* add extract-metadata step to access current changes and version ([25285a5](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/25285a5a085ebfcbefa15a26a86dd5c0e7c1079d))
* add RELEASE_AS option to .jarvis-main-release to manually set version ([f3dacb4](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f3dacb4fe7e3c4c2a8fe91e6cb01ed89a6b6cce8))
* create a GitLab release for main releases ([8d66ebd](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/8d66ebd1868b7bcb11b310aa46414359555ead88))
* refactor shallow unshallow command with !reference tag ([0f42e78](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/0f42e7806d7d7d587ba07c0f35d9ad6c849e32f7))
* remove helper image sources from repo ([f3ec146](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f3ec146868c42b4ddef34ea06b9abf790f525938))
* remove pre- prefix from pre-release tags ([0eaec7e](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/0eaec7e221965dae34f2335fb61f6f650db8950c))
* separate extract metadata script for version/changes ([28679ae](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/28679ae357188721d330eb8513d314ee971d68c8))


### Bug Fixes

* clean up standard-version CLI params ([34cc2f3](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/34cc2f3175b31817b4c08269abc64d1a4328b78d))
* move unshallow command to base release job ([d75307d](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d75307d9e32a04aeb897d5ce8e59c1e127f14f30))
* refactor jarvis-on-* templates with base template ([4dfa730](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/4dfa730f948dd2cac288139162d1d1f3bee37f44))
* simplify pre-release version generation ([e7efda0](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/e7efda0596c4631d7e438eceaefa1c28686fb2f8))

## [4.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v4.1.0...v4.2.0) (2021-03-01)


### Features

* **deploy,release:** add git-lfs support ([3b51b70](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/3b51b703548f1fc8f3ddad8d58235e6acac5b982))
* **release:** change image to one with lfs support ([d41f1d2](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d41f1d2039aea4c4e60f827666b3dc60c5e3dbe0))

## [4.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v4.0.0...v4.1.0) (2021-02-03)


### Features

* allow issue prefix and url format parameters ([9e941e9](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9e941e99c67584ad55fb17a2564aaabd7a925d0e))
* extend pre-release with issue prefix and url format parameters ([0317e56](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/0317e56e7b5cbd37f4a4385a8616c547cb5d301f))


### Bug Fixes

* changelog generation in shallow repositories ([2defad3](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/2defad3cf040dbf967c05a564c70fce03996408f))

## [4.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v3.0.1...v4.0.0) (2020-11-27)


### ⚠ BREAKING CHANGES

* enable commiting for pre-releases, making handle-pre-release unnecessary

### Features

* enable commiting for pre-releases, making handle-pre-release unnecessary ([70eb2e2](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/70eb2e2a4c4471be8db71cf970a32fd10e5184d1))
* filtering release pipeline if tip of master is already a release ([e157fff](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/e157fffa048d22d1505e5e5004014b9865707195))


### Bug Fixes

* run .jarvis-on-* for tags only ([9578fda](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9578fda01fe6bda32632a6327d4242795790137a))

### [3.0.1](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v3.0.0...v3.0.1) (2020-11-18)


### Bug Fixes

* replace problematic characters in prerelease tagname ([bf07471](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/bf07471a2f9f21f879896335360d7c19409d39bc))

## [3.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v2.0.0...v3.0.0) (2020-11-18)


### ⚠ BREAKING CHANGES

* correctly identify commits between releases

### Bug Fixes

* correctly identify commits between releases ([a2fe849](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a2fe8492bd1f3d712eb5fbffbdf7118bb9139e87))

## 2.0.0 (2020-11-05)


### ⚠ BREAKING CHANGES

* rename prerelease jobs to pre-release
* use semver-compatible pre-release versioning
* define hidden jobs only

### Features

* add dependency security scanning for java projects ([a4c4abb](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a4c4abb02b32a593f17d174e4dd4ca89d697227d))
* adds job dependency to other deploy helper images ([a62644b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a62644b6a0868f38cf7ababed81bf852963d7c4c))
* adds terraform to deploy helper image ([798c5b3](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/798c5b3a61626998fbbe33e6457ead6bcb0d2b87))
* build custom commitlint image ([b917d8f](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/b917d8f016ec93c4f2c117be3d5845788693b69e))
* creaet api-generator base image with node, java, jq ([083160b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/083160bba3607de15986be0d8f178d11f6e56f17))
* define hidden jobs only ([3158413](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/3158413de75d4543fda85f2b83d32378a9be66bb))
* dockerfile linting ([f2f393d](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f2f393d4bb6f9c72ca012fe647ea0b7285b4c748))
* jarvis based versioning and releases ([04e4407](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/04e44076303d6ecec6348b4e4ac2f5161e55c8b7))
* rename prerelease jobs to pre-release ([1fc697f](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/1fc697f93f8b644fa9794a143718fd2bfaf0cdac))
* use custom commitlint image ([a6e0da8](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a6e0da8cc0550c66e266cd4cbdfbe2296e0d0a17))
* use semver-compatible pre-release versioning ([6925c46](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/6925c46118bc0abda12aabc9ec2f62fe9db75375))


### Bug Fixes

* remove invalid rule ([9ebe291](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9ebe2910f81e7f0dcbec2a971c780f69873f169e))
* update git-push.yml command to support macOS ([b8becaf](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/b8becaf87e4d90ae7adcd9e094ff04ae70c23d30))
* updates generated url format for gitlab ([36455ba](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/36455ba62b6ba952042f2b05224441d52db4ce4e))
