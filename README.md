# JARVIS

This repository contains helper Docker images and GitLab CI scripts you can use to conveniently bootstrap the CI/CD
pipelines for your project. 

# Usage

## Including JARVIS

When hosted on gitlab.com JARVIS should always be included in your projects `.gitlab-ci.yml` by explicitly specifying a version, like this:

```
include:
  - project: team-supercharge/jarvis/jarvis
    file: jarvis.yml
    ref: <version tag>
```

when your project is hosted on a self-managed GitLab instance use the following snippet:

```
include:
  - https://gitlab.com/team-supercharge/jarvis/jarvis/raw/<version tag>/jarvis.yml
```

## Setting up the environment

The following environment variables need to be set in order for JARVIS to work:

* `JARVIS_GIT_SSH_KEY` - the SSH key JARVIS will use to commit changes to repositories; recommended to use an SSH key in Ed25519 format to fit a single variable
* `JARVIS_GIT_USER_EMAIL` - the email address used for commits
* `JARVIS_GIT_USER_NAME` - the name used for commits

The above variables should be considered to be created as global variables (in a self-managed GitLab instance) or group-level variables (on GitLab.com) to avoid unnecessary configuration steps for new projects.

## Job templates

JARVIS does not create any jobs for you, but provides convenient job templates you can use to set up the CI/CD for 
your project in a heartbeat!

You can use these templates by creating a new job in your `.gitlab-ci.yml` file and extending from one of these
templates, like this:

```
include:
  - project: team-supercharge/jarvis/jarvis
    file: jarvis.yml
    ref: <version>

your job:
    extends: .jarvis-<job-template>
```

Please check the `.gitlab-ci.yml` file in this repository for a concrete example, as JARVIS uses itself for
setting up its pipelines.

## MR jobs

### Commitlint

The `.jarvis-commitlint` job template provides a quick way to check your project's merge requests whether its
commits follow [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) standards.

## Release jobs

### Main release

The `.jarvis-main-release` job template can be used to implement a conventional commits based release workflow.
It uses the [standard version](https://github.com/conventional-changelog/standard-version) npm package to automatically
calculate your next [semantic version](https://semver.org) based on your 
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/), and generate a nice changelog.

When you trigger this job, it is going to check the commits between `HEAD` and your previous release, then write a new
version number to `package.json`, update your `CHANGELOG.md`, and create and push a new tag with the new version number.
This tag should be used to trigger your build/deploy pipeline. 

The template `.jarvis-main-release` has an option to manually specify a version to release by setting the environment
variable `RELEASE_AS` to a valid semver version number. This feature can be useful for example on a first go-live, when
the version number should be manually bumped to 1.0.0

To make your build/deploy pipeline trigger on release tags extend the `.jarvis-on-main-release`, `.jarvis-on-prerelease`
or the `.jarvis-on-release` job templates.

> ⚠️ To use this job template you need to enable the Gitlab CI deploy key *with write access* for your repository in
GitLab. You can find it under your repository's settings: `Settings > Repository > Deploy Keys`. You also need to add the *GitLab CI*
user as a project member with at least *Reporter* role.

### Prerelease

If you would also like to create intermediate releases (even from side branches) between stable releases then tou can
use the `.jarvis-prerelase` job template. This job won't increment the version number stored in your `package.json` and 
won't create any commits on your branch. It will only create and push a new tag based on your branch name. This tag
can then be used to trigger a build/deploy pipeline. 

To make your build/deploy pipeline trigger on release tags extend the `.jarvis-on-main-release`, `.jarvis-on-pre-release`
or the `.jarvis-on-release` job templates.

> ⚠️ To use this job template you need to enable the Gitlab CI deploy key *with write access* for your repository in
GitLab. You can find it under your repository's settings: `Settings > Repository > Deploy Keys`. You also need to add the *GitLab CI*
user as a project member with at least *Reporter* role.

### Extracting release metadata

In jobs extending the `.jarvis-on-*` templates (in these jobs the last commit is guaranteed to be a release commit) there
is a way to easily extract release information with [`!reference` tags](https://docs.gitlab.com/ee/ci/yaml/#reference-tags) using `!reference [.jarvis-extract-release-*]`. The extracted information can be conveniently used in release/deploy scripts, notifications, etc.  
These scripts need the `git` binary to be available.

The current version information is available both written in transient files and as environment variables:

* `.jarvis-extract-release-version` - `VERSION` file and `$VERSION` env var - the current version being built
* `.jarvis-extract-release-changes` - `CHANGES.md` file and `$CHANGES` env var - extracted content of `CHANGELOG` for the current version
* `.jarvis-extract-release-changes-since-last-deployment` - `CHANGES_SINCE_LAST_DEPLOYMENT.md` file and `CHANGES_SINCE_LAST_DEPLOYMENT` env var - extracted content of `CHANGELOG` since the previous deployment
  * in addition, this script needs the `curl` binary and `GITLAB_API_TOKEN` env var containing a Gitlab personal/project access token with API access to the project.

Usage:

```
build-job:
  extends: .jarvis-on-release
  script:
    - !reference [.jarvis-extract-release-version]
    - cat VERSION
    - echo $VERSION
    - !reference [.jarvis-extract-release-changes]
    - cat CHANGES.md
    - echo "$CHANGES" # use double quotes to preserve newlines
```

### Allow main release from side branches

If the project workflow requires it, you can tweak Jarvis to allow main releases from branches other than the default. In order to achieve this use the following syntax in your `gitlab-ci.yml` file:

```yaml
...

main-release:
  extends:
    - .jarvis-main-release
    - .jarvis-release-from-any-branch
  stage: release
  ...

pre-release:
  extends: .jarvis-pre-release
  stage: release
  ...

...
```

## Dockerfile linting

The `.jarvis-docker-lint` provides a convenient job template to lint Dockerfiles using 
[Hadolint](https://github.com/hadolint/hadolint).

To specify a Dockerfile other than the one in the repository root override the `DOCKER_FILE` variable!

Example:

```
Docker lint:
  stage: Lint
  extends: .jarvis-docker-lint
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - Dockerfile
```

## Helper images

* `deploy`: contains everything to execute a deployment on k8s (nodejs, curl, ssh, gnupg, kubectl, helm, aws cli, eksctl, terraform)
* `release`: can perform a standard version release - bumps version, generates changelog, creates git tag (nodejs, git, curl, standard-version)
* `docker-cli`: just docker-cli
* `deploy-capistrano`: builds on top of `deploy`, includes ruby and bundle for capistrano
* `deploy-serverless`: builds on top of `deploy`, includes serverless cli
* `commitlint`: node image with standard-version preinstalled

# Contributing

If you have a new idea don't be afraid to share it by submitting a pull request or asking in the #sc-ci Slack channel!

## Migration Guide

### From `5.0.0` to `6.0.0`

* with version `6.0.0` JARVIS became an open-source project 🎉
* this means you have to update your `include` references to access JARVIS from gitlab.com
* the helper Docker images are also moved under the public GitLab-hosted registry `registry.gitlab.com/team-supercharge/jarvis/images/...` so these references need to be updated as well
* as Ed25519 keys emerging the Git SSH key used by JARVIS is not anymore separated and concatenated from multiple environment variables `DEPLOY_KEY_0`, `DEPLOY_KEY_1`, etc -- instead it isbe fetched from a single env var called `JARVIS_GIT_SSH_KEY`
* an option to configure Git commit identity for releases has been added using the `JARVIS_GIT_USER_EMAIL` and `JARVIS_GIT_USER_NAME` variables

**TL;DR**
  * update your `include:` statement according to the [Including JARVIS](#including-jarvis) section
  * search for the text `registry.supercharge.io/jarvis/images` in your `.gitlab-ci.yml` file and change it to `registry.gitlab.com/team-supercharge/jarvis/images` 
  * generate a new key in Ed25519 format and store it in `JARVIS_GIT_SSH_KEY` (advised to be set globally per GitLab instance)
  * add `JARVIS_GIT_USER_EMAIL` and `JARVIS_GIT_USER_NAME` variables to configure Git commit identity (advised to be set globally per GitLab instance)

### From `4.x.x` to `5.0.0`

* the main breaking change is that previous versions of Jarvis used helper Docker images for various build steps from the internal misc/jarvis-ng or jarvis/jarvis registry paths, but those lacked proper versioning
* now these helper images are refactored into `jarvis/images` versioned semantically with Jarvis itself, and will be open-sourced in the future from this repository
* to avoid old and untracked images being used we’re going to turn off the Docker registry for `misc/jarvis-ng` and `jarvis/jarvis` on Mar 26 2021
* if you did not manually use any Docker image in your jobs e.g. `jarvis/jarvis/deploy:master` => just by setting the version number to 5.0.0 you’re good to go
* if you did use one of these helper images, please point them to `jarvis/images/<YOUR_IMAGE>:1.0.0` => this tag contains the latest version from the old registries

**TL;DR** - search for `registry.supercharge.io/jarvis/jarvis` or `registry.supercharge.io/misc/jarvis-ng`  in your `.gitlab-ci.yml`
  * if you have results migrate them to `jarvis/images/<IMAGE>:1.0.0`
  * if not, you’re good with just bumping Jarvis version

### From `3.x.x` to `4.0.0`

If you extended the `.jarvis-handle-pre-release` job in your pipeline (which did an additional pre-release calculation) please delete it and simply use the `.jarvis-main-release` and `.jarvis-pre-release` jobs in their single stage.

### From `2.x.x` to `3.0.0`

The format of the pre-release tags has been changed. If you have jobs that use regular expression triggers please update them to use this regex in case of pre-releases: `/^pre-\d+\.\d+\.\d+-.+-\b[0-9a-f]{8}\.\d+$/` or extend your job from `.jarvis-on-prerelease`.

If you use the `.jarvis-on-*` job templates to implement these triggers, you have nothing to do.

* Previous pre-release tag format: `v0.0.0-branch-commthsh`
* New pre-release tag format: `pre-0.0.0-branch-commthsh.0`

## Roadmap

* Add job templates for:
    * Dockerfile linting
    * Docker security check
    * Gradle dependency check

## Versioning

This library is developed using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and versioned 
with [standard version](https://github.com/conventional-changelog/standard-version).
